package main;


import ws.Produit;
import ws.ProduitInterface;
import ws.ProduitWSService;
import ws.ProduitWSServiceLocator;

public class Main {

	public static void main(String[] args) {
		try {

			ProduitWSService ProduitService = new ProduitWSServiceLocator();
			ProduitInterface news = ProduitService.getProduitWSPort();
			
			System.out.println("Tout");
			for(Produit p : news.findAll()) {
				System.out.println(p.getId()+" - "+p.getLibelle());
				System.out.println(p.getImage());
				System.out.println("Jour "+p.getJourDispo()+" prix "+p.getPrix()+"�");
				System.out.println("Nombre de vente "+p.getNbVente()+" avis "+p.getAvis());
			}
			System.out.println("\n");
			
			System.out.println("2019-02-05");
			for(Produit p : news.findAllDate("2019-02-05")) {
				System.out.println(p.getId()+" - "+p.getLibelle());
				System.out.println(p.getImage());
				System.out.println("Jour "+p.getJourDispo()+" prix "+p.getPrix()+"�");
				System.out.println("Nombre de vente "+p.getNbVente()+" avis "+p.getAvis());
			}
			System.out.println("\n");
			
			System.out.println("Crepe");
			for(Produit p : news.findAllLibelle("crepe")) {
				System.out.println(p.getId()+" - "+p.getLibelle());
				System.out.println(p.getImage());
				System.out.println("Jour "+p.getJourDispo()+" prix "+p.getPrix()+"�");
				System.out.println("Nombre de vente "+p.getNbVente()+" avis "+p.getAvis());
			}
			System.out.println("\n");
			
			System.out.println("D�jeuner");
			for(Produit p : news.findAllTypePlat("d�jeuner")) {
				System.out.println(p.getId()+" - "+p.getLibelle());
				System.out.println(p.getImage());
				System.out.println("Jour "+p.getJourDispo()+" prix "+p.getPrix()+"�");
				System.out.println("Nombre de vente "+p.getNbVente()+" avis "+p.getAvis());
			}
			System.out.println("\n");
			
			for(Produit p : news.findAllTypeRepas("Dessert")) {
				System.out.println(p.getId()+" - "+p.getLibelle());
				System.out.println(p.getImage());
				System.out.println("Jour "+p.getJourDispo()+" prix "+p.getPrix()+"�");
				System.out.println("Nombre de vente "+p.getNbVente()+" avis "+p.getAvis());
			}
			System.out.println("\n");
			
			System.out.println("D�jeuner et Dessert");
			for(Produit p : news.findAllTypeRepasTypePlat("Dessert", "D�jeuner")) {
				System.out.println(p.getId()+" - "+p.getLibelle());
				System.out.println(p.getImage());
				System.out.println("Jour "+p.getJourDispo()+" prix "+p.getPrix()+"�");
				System.out.println("Nombre de vente "+p.getNbVente()+" avis "+p.getAvis());
			}
			System.out.println("\n");


		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
