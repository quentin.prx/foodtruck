package ws;

public class ProduitInterfaceProxy implements ws.ProduitInterface {
  private String _endpoint = null;
  private ws.ProduitInterface produitInterface = null;
  
  public ProduitInterfaceProxy() {
    _initProduitInterfaceProxy();
  }
  
  public ProduitInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initProduitInterfaceProxy();
  }
  
  private void _initProduitInterfaceProxy() {
    try {
      produitInterface = (new ws.ProduitWSServiceLocator()).getProduitWSPort();
      if (produitInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)produitInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)produitInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (produitInterface != null)
      ((javax.xml.rpc.Stub)produitInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.ProduitInterface getProduitInterface() {
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface;
  }
  
  public ws.Produit[] findAllTypeRepasTypePlat(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAllTypeRepasTypePlat(arg0, arg1);
  }
  
  public ws.Produit findById(int arg0) throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findById(arg0);
  }
  
  public ws.Produit[] findAllBestVente() throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAllBestVente();
  }
  
  public ws.Produit[] findAllDate(java.lang.String arg0) throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAllDate(arg0);
  }
  
  public ws.Produit[] findAllLibelle(java.lang.String arg0) throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAllLibelle(arg0);
  }
  
  public ws.Produit[] findAllTypePlat(java.lang.String arg0) throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAllTypePlat(arg0);
  }
  
  public ws.Produit[] findAll() throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAll();
  }
  
  public ws.Produit[] findAllTypeRepas(java.lang.String arg0) throws java.rmi.RemoteException{
    if (produitInterface == null)
      _initProduitInterfaceProxy();
    return produitInterface.findAllTypeRepas(arg0);
  }
  
  
}