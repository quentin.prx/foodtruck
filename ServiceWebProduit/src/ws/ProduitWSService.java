/**
 * ProduitWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface ProduitWSService extends javax.xml.rpc.Service {
    public java.lang.String getProduitWSPortAddress();

    public ws.ProduitInterface getProduitWSPort() throws javax.xml.rpc.ServiceException;

    public ws.ProduitInterface getProduitWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
