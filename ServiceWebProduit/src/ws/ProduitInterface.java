/**
 * ProduitInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface ProduitInterface extends java.rmi.Remote {
    public ws.Produit[] findAllTypeRepasTypePlat(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public ws.Produit findById(int arg0) throws java.rmi.RemoteException;
    public ws.Produit[] findAllBestVente() throws java.rmi.RemoteException;
    public ws.Produit[] findAllDate(java.lang.String arg0) throws java.rmi.RemoteException;
    public ws.Produit[] findAllLibelle(java.lang.String arg0) throws java.rmi.RemoteException;
    public ws.Produit[] findAllTypePlat(java.lang.String arg0) throws java.rmi.RemoteException;
    public ws.Produit[] findAll() throws java.rmi.RemoteException;
    public ws.Produit[] findAllTypeRepas(java.lang.String arg0) throws java.rmi.RemoteException;
}
