package ws;

public class NewsInterfaceProxy implements ws.NewsInterface {
  private String _endpoint = null;
  private ws.NewsInterface newsInterface = null;
  
  public NewsInterfaceProxy() {
    _initNewsInterfaceProxy();
  }
  
  public NewsInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initNewsInterfaceProxy();
  }
  
  private void _initNewsInterfaceProxy() {
    try {
      newsInterface = (new ws.NewsWSServiceLocator()).getNewsWSPort();
      if (newsInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)newsInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)newsInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (newsInterface != null)
      ((javax.xml.rpc.Stub)newsInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.NewsInterface getNewsInterface() {
    if (newsInterface == null)
      _initNewsInterfaceProxy();
    return newsInterface;
  }
  
  public ws.Produit[] findAllBestVente() throws java.rmi.RemoteException{
    if (newsInterface == null)
      _initNewsInterfaceProxy();
    return newsInterface.findAllBestVente();
  }
  
  public ws.News[] findAllValide() throws java.rmi.RemoteException{
    if (newsInterface == null)
      _initNewsInterfaceProxy();
    return newsInterface.findAllValide();
  }
  
  public ws.News[] findAll() throws java.rmi.RemoteException{
    if (newsInterface == null)
      _initNewsInterfaceProxy();
    return newsInterface.findAll();
  }
  
  public ws.News findById(int arg0) throws java.rmi.RemoteException{
    if (newsInterface == null)
      _initNewsInterfaceProxy();
    return newsInterface.findById(arg0);
  }
  
  
}