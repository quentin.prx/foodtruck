/**
 * NewsWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class NewsWSServiceLocator extends org.apache.axis.client.Service implements ws.NewsWSService {

    public NewsWSServiceLocator() {
    }


    public NewsWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public NewsWSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for NewsWSPort
    private java.lang.String NewsWSPort_address = "http://localhost:4798/ws/news";

    public java.lang.String getNewsWSPortAddress() {
        return NewsWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String NewsWSPortWSDDServiceName = "NewsWSPort";

    public java.lang.String getNewsWSPortWSDDServiceName() {
        return NewsWSPortWSDDServiceName;
    }

    public void setNewsWSPortWSDDServiceName(java.lang.String name) {
        NewsWSPortWSDDServiceName = name;
    }

    public ws.NewsInterface getNewsWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(NewsWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getNewsWSPort(endpoint);
    }

    public ws.NewsInterface getNewsWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.NewsWSPortBindingStub _stub = new ws.NewsWSPortBindingStub(portAddress, this);
            _stub.setPortName(getNewsWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setNewsWSPortEndpointAddress(java.lang.String address) {
        NewsWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.NewsInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.NewsWSPortBindingStub _stub = new ws.NewsWSPortBindingStub(new java.net.URL(NewsWSPort_address), this);
                _stub.setPortName(getNewsWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("NewsWSPort".equals(inputPortName)) {
            return getNewsWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/", "NewsWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/", "NewsWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("NewsWSPort".equals(portName)) {
            setNewsWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
