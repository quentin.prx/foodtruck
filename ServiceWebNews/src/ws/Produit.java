/**
 * Produit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class Produit  implements java.io.Serializable {
    private double avis;

    private int id;

    private java.lang.String image;

    private java.lang.String jourDispo;

    private java.lang.String libelle;

    private int nbVente;

    private double prix;

    private java.lang.String typePlat;

    private java.lang.String typeRepas;

    public Produit() {
    }

    public Produit(
           double avis,
           int id,
           java.lang.String image,
           java.lang.String jourDispo,
           java.lang.String libelle,
           int nbVente,
           double prix,
           java.lang.String typePlat,
           java.lang.String typeRepas) {
           this.avis = avis;
           this.id = id;
           this.image = image;
           this.jourDispo = jourDispo;
           this.libelle = libelle;
           this.nbVente = nbVente;
           this.prix = prix;
           this.typePlat = typePlat;
           this.typeRepas = typeRepas;
    }


    /**
     * Gets the avis value for this Produit.
     * 
     * @return avis
     */
    public double getAvis() {
        return avis;
    }


    /**
     * Sets the avis value for this Produit.
     * 
     * @param avis
     */
    public void setAvis(double avis) {
        this.avis = avis;
    }


    /**
     * Gets the id value for this Produit.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this Produit.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the image value for this Produit.
     * 
     * @return image
     */
    public java.lang.String getImage() {
        return image;
    }


    /**
     * Sets the image value for this Produit.
     * 
     * @param image
     */
    public void setImage(java.lang.String image) {
        this.image = image;
    }


    /**
     * Gets the jourDispo value for this Produit.
     * 
     * @return jourDispo
     */
    public java.lang.String getJourDispo() {
        return jourDispo;
    }


    /**
     * Sets the jourDispo value for this Produit.
     * 
     * @param jourDispo
     */
    public void setJourDispo(java.lang.String jourDispo) {
        this.jourDispo = jourDispo;
    }


    /**
     * Gets the libelle value for this Produit.
     * 
     * @return libelle
     */
    public java.lang.String getLibelle() {
        return libelle;
    }


    /**
     * Sets the libelle value for this Produit.
     * 
     * @param libelle
     */
    public void setLibelle(java.lang.String libelle) {
        this.libelle = libelle;
    }


    /**
     * Gets the nbVente value for this Produit.
     * 
     * @return nbVente
     */
    public int getNbVente() {
        return nbVente;
    }


    /**
     * Sets the nbVente value for this Produit.
     * 
     * @param nbVente
     */
    public void setNbVente(int nbVente) {
        this.nbVente = nbVente;
    }


    /**
     * Gets the prix value for this Produit.
     * 
     * @return prix
     */
    public double getPrix() {
        return prix;
    }


    /**
     * Sets the prix value for this Produit.
     * 
     * @param prix
     */
    public void setPrix(double prix) {
        this.prix = prix;
    }


    /**
     * Gets the typePlat value for this Produit.
     * 
     * @return typePlat
     */
    public java.lang.String getTypePlat() {
        return typePlat;
    }


    /**
     * Sets the typePlat value for this Produit.
     * 
     * @param typePlat
     */
    public void setTypePlat(java.lang.String typePlat) {
        this.typePlat = typePlat;
    }


    /**
     * Gets the typeRepas value for this Produit.
     * 
     * @return typeRepas
     */
    public java.lang.String getTypeRepas() {
        return typeRepas;
    }


    /**
     * Sets the typeRepas value for this Produit.
     * 
     * @param typeRepas
     */
    public void setTypeRepas(java.lang.String typeRepas) {
        this.typeRepas = typeRepas;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Produit)) return false;
        Produit other = (Produit) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.avis == other.getAvis() &&
            this.id == other.getId() &&
            ((this.image==null && other.getImage()==null) || 
             (this.image!=null &&
              this.image.equals(other.getImage()))) &&
            ((this.jourDispo==null && other.getJourDispo()==null) || 
             (this.jourDispo!=null &&
              this.jourDispo.equals(other.getJourDispo()))) &&
            ((this.libelle==null && other.getLibelle()==null) || 
             (this.libelle!=null &&
              this.libelle.equals(other.getLibelle()))) &&
            this.nbVente == other.getNbVente() &&
            this.prix == other.getPrix() &&
            ((this.typePlat==null && other.getTypePlat()==null) || 
             (this.typePlat!=null &&
              this.typePlat.equals(other.getTypePlat()))) &&
            ((this.typeRepas==null && other.getTypeRepas()==null) || 
             (this.typeRepas!=null &&
              this.typeRepas.equals(other.getTypeRepas())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Double(getAvis()).hashCode();
        _hashCode += getId();
        if (getImage() != null) {
            _hashCode += getImage().hashCode();
        }
        if (getJourDispo() != null) {
            _hashCode += getJourDispo().hashCode();
        }
        if (getLibelle() != null) {
            _hashCode += getLibelle().hashCode();
        }
        _hashCode += getNbVente();
        _hashCode += new Double(getPrix()).hashCode();
        if (getTypePlat() != null) {
            _hashCode += getTypePlat().hashCode();
        }
        if (getTypeRepas() != null) {
            _hashCode += getTypeRepas().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Produit.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/", "produit"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("image");
        elemField.setXmlName(new javax.xml.namespace.QName("", "image"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jourDispo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jourDispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("libelle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "libelle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nbVente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nbVente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prix");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typePlat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typePlat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeRepas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeRepas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
