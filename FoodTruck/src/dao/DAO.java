package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class DAO<T,K> {
	  protected Connection conn = null;
	   
	  public DAO(Connection conn){
	    this.conn = conn;
	  }
	  
	  public void seDeconnecterDAO() {
		  try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  }
	  //CRRUD
	public abstract void create(T t);
	public abstract T findById(K id);
	public abstract List<T> findAll();
	public abstract void update(T ob);
	public abstract void delete(K id);
}