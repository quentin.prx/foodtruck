package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.News;

public class DAONews extends DAO<News,Integer>{

	public DAONews(Connection conn) {
		super(conn);
	}

	@Override
	public void create(News t) {
		try {
			PreparedStatement ps=conn.prepareStatement("INSERT INTO news (titre,description,image,dateFin) VALUES (?,?,?,?)");
			ps.setString(1,t.getTitre());
			ps.setString(2,t.getDescription());
			ps.setString(3, t.getImage());
			ps.setString(4, t.getDateFin());
			ps.executeUpdate();
			ps.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public News findById(Integer id) {
		News n = null;
		try {
			String sql = "SELECT * FROM News where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				n = new News(rs.getInt("id"),rs.getString("titre"),rs.getString("description"),rs.getString("image"),rs.getString("dateFin"));	
			}

			rs.close();
			ps.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<News> findAll() {
		List<News> listNews = new ArrayList<News>();
		try {
			String sql = "SELECT * FROM news";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				listNews.add(new News(rs.getInt("id"),rs.getString("titre"),rs.getString("description"),rs.getString("image"),rs.getString("dateFin")));		
			}

			rs.close();
			ps.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return listNews;
	}

	
	
	public List<News> findAllValide() {
		List<News> listNews = new ArrayList<News>();
		try {
			String sql = "SELECT * FROM news where CURRENT_DATE < dateFin ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				listNews.add(new News(rs.getInt("id"),rs.getString("titre"),rs.getString("description"),rs.getString("image"),rs.getString("dateFin")));		
			}

			rs.close();
			ps.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return listNews;
	}

	
	@Override
	public void update(News n) {
		try {
			PreparedStatement ps=conn.prepareStatement("UPDATE news SET titre=?, description=?, image=?,dateFin=? where id=?");
	        ps.setString(1,n.getTitre());
	        ps.setString(2,n.getDescription());
	        ps.setString(3, n.getImage());
	        ps.setString(4, n.getDateFin());
	        ps.setInt(5, n.getId());
	        ps.executeUpdate();
	        ps.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Integer id) {
		try {
			PreparedStatement ps=conn.prepareStatement("DELETE FROM news where id=?");
	        ps.setInt(1,id);
	        ps.executeUpdate();
	        ps.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
