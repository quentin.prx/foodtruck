package model;

public class Produit {
	private int id;
	private String libelle;
	private double prix;
	private double avis;
	private int nbVente;
	private String typePlat;
	private String typeRepas;
	private String image;
	private String jourDispo;
	
	public Produit(int id, String libelle, double prix, double avis, int nbVente, String typePlat, String typeRepas,
			String image, String jourDispo) {
		this.id = id;
		this.libelle = libelle;
		this.prix = prix;
		this.avis = avis;
		this.nbVente = nbVente;
		this.typePlat = typePlat;
		this.typeRepas = typeRepas;
		this.image = image;
		this.jourDispo = jourDispo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public double getAvis() {
		return avis;
	}

	public void setAvis(double avis) {
		this.avis = avis;
	}

	public int getNbVente() {
		return nbVente;
	}

	public void setNbVente(int nbVente) {
		this.nbVente = nbVente;
	}

	public String getTypePlat() {
		return typePlat;
	}

	public void setTypePlat(String typePlat) {
		this.typePlat = typePlat;
	}

	public String getTypeRepas() {
		return typeRepas;
	}

	public void setTypeRepas(String typeRepas) {
		this.typeRepas = typeRepas;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getJourDispo() {
		return jourDispo;
	}

	public void setJourDispo(String jourDispo) {
		this.jourDispo = jourDispo;
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", libelle=" + libelle + ", prix=" + prix + ", avis=" + avis + ", nbVente="
				+ nbVente + ", typePlat=" + typePlat + ", typeRepas=" + typeRepas + ", image=" + image + ", jourDispo="
				+ jourDispo + "]";
	}
	
	
}
