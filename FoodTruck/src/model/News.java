package model;

public class News {
	private int id;
	private String titre;
	private String description;
	private String image;
	private String dateFin;
	
	public News(int id, String titre, String description, String image,String dateFin) {
		this.id = id;
		this.titre = titre;
		this.description = description;
		this.image = image;
		this.dateFin= dateFin;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", titre=" + titre + ", description=" + description + ", image=" + image
				+ ", dateFin=" + dateFin + "]";
	}

	
	
	
	
	
	
}
