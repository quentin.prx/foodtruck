package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Produit;

@WebService
public interface ProduitInterface {

	@WebMethod
	public Produit findById(int d);
	
	@WebMethod
	public List<Produit> findAll();
	
	@WebMethod
	public List<Produit> findAllTypeRepas(String typeRepas);
	
	@WebMethod
	public List<Produit> findAllTypePlat(String typePlat);
	
	@WebMethod
	public List<Produit> findAllTypeRepasTypePlat(String typeRepas, String typePlat);
	
	@WebMethod
	public List<Produit> findAllLibelle(String libelle);
	
	@WebMethod
	public List<Produit> findAllDate(String date);
	
	@WebMethod
	public List<Produit> findAllBestVente();
	
}
