package ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import model.News;
import model.Produit;

@WebService
public interface NewsInterface {

	@WebMethod
	public News findById(int id);
	
	@WebMethod
	public List<News> findAll();
	
	@WebMethod
	public List<News> findAllValide();
	
	@WebMethod
	public List<Produit> findAllBestVente();
}
