package ws;

import java.util.List;

import javax.jws.*;

import dao.Connexion;
import dao.DAOProduit;
import model.Produit;

@WebService(endpointInterface = "ws.ProduitInterface")
public class ProduitWS implements ProduitInterface {
	DAOProduit daoP = new DAOProduit(Connexion.getInstance());

	@Override
	public Produit findById(int d) {
		return daoP.findById(d);
	}

	@Override
	public List<Produit> findAll() {
		return daoP.findAll();
	}

	@Override
	public List<Produit> findAllTypeRepas(String typeRepas) {
		return daoP.findAllTypeRepas(typeRepas);
	}

	@Override
	public List<Produit> findAllTypePlat(String typePlat) {
		return daoP.findAllTypePlat(typePlat);
	}
	

	@Override
	public List<Produit> findAllLibelle(String libelle) {
		return daoP.findAllLibelle(libelle);
	}

	@Override
	public List<Produit> findAllDate(String date) {
		return daoP.findAllDate(date);
	}

	@Override
	public List<Produit> findAllBestVente() {
		return daoP.findAllBestVente();
	}

	@Override
	public List<Produit> findAllTypeRepasTypePlat(String typeRepas, String typePlat) {
		return daoP.findAllTypeRepasTypePlat(typeRepas, typePlat);
	}

}
