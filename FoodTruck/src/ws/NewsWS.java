package ws;

import java.util.List;

import javax.jws.WebService;

import dao.Connexion;
import dao.DAONews;
import dao.DAOProduit;
import model.News;
import model.Produit;

@WebService(endpointInterface = "ws.NewsInterface")
public class NewsWS implements NewsInterface {
	DAONews daoN = new DAONews(Connexion.getInstance());
	DAOProduit daoP = new DAOProduit(Connexion.getInstance());

	@Override
	public News findById(int id) {
		return daoN.findById(id);
	}

	@Override
	public List<News> findAll() {
		return daoN.findAll();
	}

	@Override
	public List<Produit> findAllBestVente() {
		return daoP.findAllBestVente();
	}

	@Override
	public List<News> findAllValide() {
		// TODO Auto-generated method stub
		return daoN.findAllValide();
	}

}
