package main;


import javax.xml.ws.Endpoint;

import ws.NewsWS;
import ws.ProduitWS;

public class Main {

	public static void main(String[] args) {
		
		try {
			Endpoint.publish("http://localhost:4797/ws/product", new ProduitWS());
			Endpoint.publish("http://localhost:4798/ws/news", new NewsWS());
			System.out.println("Done");
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
